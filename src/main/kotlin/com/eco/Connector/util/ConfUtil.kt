package com.eco.Connector.util

import com.eco.Connector.bean.input
import com.eco.Connector.bean.outDoc
import com.eco.Connector.bean.sender
import com.mchange.v2.lang.StringUtils
import com.moandjiezana.toml.Toml
import java.util.*


class ConfUtil {
    companion object {
        val PROJECT_NAME = "Connector"
        var LOG_PATH = ""
        var CRON_EXP = ""
            private set

        //sender
        lateinit var SENDER: sender
        //OutPut
        var OUT_FOLDER_LIST = ArrayList <outDoc> ()
            private set
        //input
        lateinit var INPUT: input

        fun configure(tomlFile:Toml){
            LOG_PATH = tomlFile.getString("LogPath")
            SENDER = tomlFile.getTable("Sender").to(sender::class.java)
            if(!StringUtils.nonEmptyString(SENDER.CronExpression)){
                // run once by one hour
                SENDER.CronExpression = "0 * 8-20 * * ?"
            }
            INPUT = tomlFile.getTable("Input").to(input::class.java)
            for( hmData  in tomlFile.getTables("Output")){
                OUT_FOLDER_LIST.add(hmData.to(outDoc::class.java))
            }

            println("###################"+PROJECT_NAME+" Configure###################")
            println("PROJECT_NAME = " + PROJECT_NAME)
            println("LOG_PATH = " + LOG_PATH)
            println("CHECK_KEY = " + SENDER.CheckKey)
            println("COMPANY_DUNS = " + SENDER.CompanyDuns)
            println("INPUT_URL = " + SENDER.InputUrl)
            println("OUTPUT_URL = " + SENDER.OutputUrl)
            println("X_API_KEY = " + SENDER.AwsApiKey)
            println("######################################")
            System.setProperty("log.logpath",ConfUtil.LOG_PATH);
            System.setProperty("cron.expression",SENDER.CronExpression);
        }
    }
}