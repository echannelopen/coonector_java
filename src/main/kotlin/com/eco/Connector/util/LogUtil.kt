package com.eco.Connector.util

import org.apache.commons.lang.time.DateFormatUtils
import org.slf4j.Logger

import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

    fun getStackNo(funName: String): Int {
        //String returnString = null;
        val sw = StringWriter()
        Throwable().printStackTrace(PrintWriter(sw))
        val callStack = sw.toString()
        var startPos = callStack.indexOf(funName)
        val endPos = callStack.indexOf("java.lang.reflect.Method.invoke")
        var result = 0
        while (true) {
            startPos = callStack.indexOf("\n\tat", startPos + 1)
            if (startPos < 0 || startPos > endPos) break
            result++
        }
        return result - 1
    }

    fun getClassName():String{
        var longName = getFuncInfo("getClassName")
        return longName.substring(0,longName.lastIndexOf("."))
    }

    fun getFuncInfo(funName: String): String {
        var returnString: String? = null
        val sw = StringWriter()
        Throwable().printStackTrace(PrintWriter(sw))
        val callStack = sw.toString()
        var startPos = callStack.indexOf(funName)
        startPos = callStack.indexOf("\n\tat", startPos)
        val endPos = callStack.indexOf("(", startPos)
        returnString = callStack.substring(startPos + 5, endPos)
        return returnString
    }

    fun logSysOut(`object`: Any) {
        //只有這個System.out
        println(`object`)
    }

    fun logFuncStart(logger: Logger): Long {
        val num = getStackNo("logFuncStart")
        val sTime =  Date()
        val sMsg = num.toString() + "." + "-> : Execute " + getFuncInfo("logFuncStart") + " Start. [Time]:" + sTime + "\n"
        logger.info(sMsg)
        return System.currentTimeMillis()
    }

    fun logFuncEnd(logger: Logger) {
        val num = getStackNo("logFuncEnd")
        val sTime = Date()
        val sMsg = num.toString() + "." + "-> : Execute " + getFuncInfo("logFuncEnd") + " End. [Time]:" + sTime

        logger.info(sMsg)
        logSysOut(sMsg)
    }

   fun logFuncEnd(logger: Logger, lStartTime: Long) {
        val num = getStackNo("logFuncEnd")
        val sTime = DateFormatUtils.format(Date(), "yyyy-MM-dd HH:mm:ss")
        var sMsg = num.toString() + "." + "-> : Execute " + getFuncInfo("logFuncEnd") + " End. [Time]:" + sTime
        val lCostTime = System.currentTimeMillis() - lStartTime
        sMsg += ",cost time:$lCostTime"
       logger.info(sMsg)
    }


