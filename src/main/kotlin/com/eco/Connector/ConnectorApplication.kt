package com.eco.Connector

import com.eco.Connector.util.ConfUtil
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling
import com.moandjiezana.toml.Toml
import java.io.File

@SpringBootApplication
@EnableScheduling
class ConnectorApplication


fun main(args: Array<String>) {
	config()
	runApplication<ConnectorApplication>(*args)
}

fun config(){
    //config setting
    // prod :production
    val fProp = File(".\\config.toml")

    //debug : 開發用
    //val fProp = File(System.getProperty("user.dir")+File.separator+"conf"+File.separator+"config.toml")

    val toml = Toml().parse(fProp)
    ConfUtil.configure(toml)
}



