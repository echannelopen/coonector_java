package com.eco.Connector.job

import com.eco.Connector.bean.outDoc
import com.eco.Connector.util.ConfUtil
import com.eco.Connector.util.logFuncEnd
import com.eco.Connector.util.logFuncStart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.io.File

import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.PostMethod
import org.apache.commons.httpclient.methods.multipart.FilePart
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity
import org.apache.commons.httpclient.methods.multipart.Part
import org.apache.commons.httpclient.methods.multipart.StringPart
import org.apache.commons.io.FileUtils
import org.apache.commons.lang.StringUtils
import org.slf4j.LoggerFactory
import java.net.HttpURLConnection
import java.util.*
import java.text.SimpleDateFormat



@Component
class putFileToConnector{
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    @Scheduled(cron="\${cron.expression}")
    fun excute() {
        var begin = logFuncStart(logger)
        try{
            for(  folder in ConfUtil.OUT_FOLDER_LIST){
                getFileFromOutFolder(folder)
            }
        }catch (e: Exception){
            logger!!.error(e.message)
        }
        logFuncEnd(this!!.logger!!,begin)
    }

    fun getFileFromOutFolder(outdoc : outDoc){
        try{
            listFolderFiles(outdoc)
        }catch (e:Exception) {
            e.printStackTrace()
            throw e
        }
    }

    fun listFolderFiles(outdoc : outDoc) {
        var folder = File(outdoc.FolderPath)
        logger.info("[folder]: "+outdoc.FolderPath)
        for (fileEntry in folder.listFiles()!!) {
            try{
                if (fileEntry.isDirectory) {
                    // pass 不然怕會做不完
                    logger.info(fileEntry.name +" is Directory!!")
                } else {
                    logger.info("[File Counts]: "+folder.list().size)
                    if(!StringUtils.isBlank(outdoc.Extension) && outdoc.Extension.replace(".","").equals(fileEntry.extension)){
                        postFileToS3(fileEntry ,outdoc)
                    }else{
                        logger.info("putFIleError: [Config Extension]: "+outdoc.Extension +" ,[fileEntry Extension]: "+fileEntry.extension)
                    }
                }
            }catch (e : Exception){
                e.printStackTrace()
                continue;
            }
        }
    }

    fun postFileToS3(fileEntry:File , outDoc : outDoc){
        var httpPost = PostMethod(ConfUtil.SENDER.OutputUrl)
        try{
            logger.info("[fromDuns]: "+ConfUtil.SENDER.CompanyDuns)
            logger.info("[toDuns]: "+outDoc.ToDuns)
            logger.info("[checkKey]: "+ConfUtil.SENDER.CheckKey)
            logger.info("[docType]: "+outDoc.DocType)
            logger.info("[fileName]: "+fileEntry.name)

            var parts = arrayOf<Part>(
                    FilePart("data", fileEntry),
                    StringPart("fromDuns", ConfUtil.SENDER.CompanyDuns),
                    StringPart("toDuns",outDoc.ToDuns ),
                    StringPart("checkKey", ConfUtil.SENDER.CheckKey ),
                    StringPart("docKey", fileEntry.nameWithoutExtension ),
                    StringPart("docType",outDoc.DocType ))

            httpPost.setRequestEntity(MultipartRequestEntity(parts, httpPost.getParams()))
            var client = HttpClient()
            httpPost.setRequestHeader("x-api-key", ConfUtil.SENDER.AwsApiKey)
            var iStatus = client.executeMethod(httpPost)
            logger.info("postFileToS3 StatusCode : "+iStatus)

            var sSysDate = SimpleDateFormat("yyyyMMddHHmmss").format(Date())
            var activeFilePath = outDoc.ArchiveFolder+File.separator+sSysDate.substring(0,4)+File.separator+sSysDate.substring(4,6)+File.separator+sSysDate.substring(6,8)
            if(iStatus == HttpURLConnection.HTTP_OK){
                // move file to active Folder
                var fileDest = File(activeFilePath+File.separator+fileEntry.name)
                if(fileDest.exists() && !fileDest.isDirectory) {
                    fileDest = File(activeFilePath+File.separator+sSysDate+"_"+fileEntry.name)
                    FileUtils.copyFile(fileEntry, fileDest);
                    FileUtils.deleteQuietly(fileEntry)
                }else{
                    FileUtils.moveFileToDirectory(fileEntry, File(activeFilePath) , true)
                }
            }else{
                // error
                val responseMsg = httpPost.getResponseBodyAsString()
                logger.error("postFileToS3 error StatusCode: "+iStatus+"errorMessage:"+responseMsg)
            }
        }catch (e:Exception){
            throw  e
        }finally {
            httpPost.releaseConnection()
        }
    }
}