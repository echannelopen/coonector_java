package com.eco.Connector.job

import com.eco.Connector.util.ConfUtil
import com.eco.Connector.util.logFuncEnd
import com.eco.Connector.util.logFuncStart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.io.File

import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.GetMethod
import org.apache.commons.io.IOUtils
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption

@Component
class getFileFromConnector{
        private val logger = LoggerFactory.getLogger(this.javaClass)

        @Autowired
        @Scheduled(cron="\${cron.expression}")
        fun excute() {
            var begin = logFuncStart(logger)
            try {
                getFileFromSqs()
            } catch (e: Exception) {
                logger!!.error(e.message)
            }
            logFuncEnd(this!!.logger!!, begin)
        }

        fun getFileFromSqs() {
            var bHaveData = true
            var sInputUrl: String = ConfUtil.SENDER.InputUrl+"?duns="+ConfUtil.SENDER.CompanyDuns+"&checkKey="+ConfUtil.SENDER.CheckKey
            logger.info("[Input_Url]: "+sInputUrl)
            val method = GetMethod(sInputUrl)
            try {
                while(bHaveData){
                    var client = HttpClient()
                    method.setRequestHeader("x-api-key", ConfUtil.SENDER.AwsApiKey)
                    method.setRequestHeader("x-api-from", ConfUtil.INPUT.ApiFrom)
                    var iStatus = client.executeMethod(method)
                    if(iStatus==200){
                        var jsoResp:JSONObject = JSONObject(method.getResponseBodyAsString())
                        if(jsoResp.has("sqsMessage")){
                            var jsoSqsMsg = jsoResp.getJSONObject("sqsMessage")
                            var sFileUrl:String = jsoSqsMsg.getString("fileContent")
                            var jsoResponseInfo = jsoSqsMsg.getJSONObject("info")
                            var sFilePath = jsoSqsMsg.getString("filePath")

                            var sFromDuns = jsoResponseInfo.getString("fromDuns")
                            var sDocType = jsoResponseInfo.getString("docType")
                            var sFileName = sFilePath.substring(sFilePath.lastIndexOf("/"))
                            logger.info("[fromDuns]: "+sFromDuns)
                            logger.info("[docType]: "+sDocType)
                            logger.info("[filePath]: "+sFilePath)
                            logger.info("[fileName]: "+sFileName)

                            //var inputStream =  URL(fileUrl).openStream()
                            var sLocalFilePath = ConfUtil.INPUT.FolderPath+File.separator+sFromDuns+File.separator+sDocType
                            var fLocalPath =  File(sLocalFilePath)
                            if(!fLocalPath.exists()) fLocalPath.mkdirs()

                            var sLocalFileName = sLocalFilePath+File.separator+sFileName

                            var fLocalFile =  File(sLocalFileName)
                            if(fLocalFile.exists()) {
                                sFileName = fLocalFile.nameWithoutExtension+"_"+System.currentTimeMillis()+"."+fLocalFile.extension
                                sLocalFileName = sLocalFilePath+File.separator+sFileName
                            }

                            URL(sFileUrl).openStream().use {
                                Files.copy(it,Paths.get(sLocalFileName),StandardCopyOption.REPLACE_EXISTING)
                                it.bufferedReader().readLines()
                            }
                            val jsoDeleteInfo = JSONObject()
                            jsoDeleteInfo.put("duns",ConfUtil.SENDER.CompanyDuns)
                            jsoDeleteInfo.put("checkKey",ConfUtil.SENDER.CheckKey)
                            jsoDeleteInfo.put("messageId",jsoSqsMsg.getString("messageId"))
                            jsoDeleteInfo.put("receiptHandle",jsoSqsMsg.getString("receiptHandle"))

                            deleSQS(jsoDeleteInfo.toString())
                        }else{
                            bHaveData = false
                        }

                    }else{
                        bHaveData = false
                        logger.error(method.getResponseBodyAsString())
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                logger.error(e.message)
            }finally {
                method.releaseConnection()
            }
        }

        fun deleSQS(sInput:String){
            val url = URL(ConfUtil.SENDER.InputUrl)
            val conn = url.openConnection() as HttpURLConnection
            try{
                conn.setRequestMethod("DELETE")
                conn.setRequestProperty("Content-Type", MediaType.APPLICATION_JSON_UTF8_VALUE)
                conn.setReadTimeout(30 * 1000)// give it iTimeOut seconds to respond
                conn.setRequestProperty("x-api-key", ConfUtil.SENDER.AwsApiKey)
                conn.setDoOutput(true);
                val  os = conn.getOutputStream();
                os.write(sInput.toByteArray());
                os.flush();
                os.close();

                val iResponseCode = conn.responseCode
                logger.info("Delete Queue Complete StatusCode: " + iResponseCode)
                if (iResponseCode === HttpURLConnection.HTTP_OK) {
                    logger.info(IOUtils.toString(conn.inputStream, "UTF-8"))
                } else  {
                    logger.debug("HttpURLConnection error: " + iResponseCode)
                    val errorStream = conn.getErrorStream()
                    logger.error("errorCode :"+ iResponseCode + "Msg : " +errorStream.bufferedReader().use { it.readText() })
                }
            } catch (e: Exception) {
                e.printStackTrace()
                logger.error(e.message)
            }finally{
                if(conn!=null){
                    conn.disconnect()
                }
            }

        }
}