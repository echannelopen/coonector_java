# O2O Project #

for Connector


### Requirements ###

* JDK 1.8
* gradle

### 開發環境建置 ###

* git clone :
* IDE : IntelliJ IDEA
* copy conf\config_example.toml -> rename **config.toml**
* 執行以下script

>gradlew build

* find **coonector_java.jar** from .\build\libs\

### production建置 ###
* 將上述提到 **config.toml**、**coonector_java.jar** 放置同一層資料夾
* 設置 config.toml內各項參數與排程設定
* 執行以下 startO2O.bat

>java -jar coonector_java.jar

### 排程設定 ###
* 使用 **cron expression**
